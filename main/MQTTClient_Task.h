#ifndef MQTT_CLIENT_TASK
#define MQTT_CLIENT_TASK

#include "led_strip.h"
#include "mqtt_client.h"

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

typedef struct MQTTClient_Config
{
    char* SSID;
    char* PSK;
    char* Host;
    char* Username;
    char* Password;
    char* ID;

    // Effect Queue
    QueueHandle_t* Effect_Queue;
} MQTTClient_Config;

typedef struct MQTTClient_Data
{
    esp_mqtt_client_handle_t client;

    char* ID;
    char* Topic;

    // Effect Queue
    QueueHandle_t* Effect_Queue;
} MQTTClient_Data;

void MQTTClient_Init(MQTTClient_Config* config, MQTTClient_Data* data);
void MQTTClient_Task(void *pvParameter);

#endif