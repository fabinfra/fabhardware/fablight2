#ifndef LED_CONTROLLER_TASK1
#define LED_CONTROLLER_TASK1

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "led_strip.h"

enum LEDController_Effect
{
    STATIC = 0,
};

typedef struct LEDController_Config
{
    // Number of Rings
    int Ring_Count;
    // Number of LEDs per Ring
    int LED_Count;
    // Number of Stripes
    int Segment_Count;
    // Pins of Stripes
    int* Segment_Pins;

    // Effect Queue
    QueueHandle_t* Effect_Queue;
} LEDController_Config;

typedef struct LEDController_Data
{
    // Number of Stripes
    int LED_Handle_Count;

    // LED Stripe Handle
    led_strip_handle_t* LED_Handles;
} LEDController_Data;

typedef struct LEDController_Command
{
    // Effect
    enum LEDController_Effect Effect;

    // Segment, -1 are ALL
    int Segment;

    // Ring, -1 are ALL
    int Ring;

    // Colors
    uint8_t Red;
    uint8_t Green;
    uint8_t Blue;
} LEDController_Command;

void LEDController_Init(LEDController_Config* config, LEDController_Data* data);
void LEDController_Task(void *pvParameter);

#endif