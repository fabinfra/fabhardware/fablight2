#include "MQTTClient_Task.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include <string.h>

#include "esp_log.h"
#include "esp_wifi.h"
#include "mqtt_client.h"
#include "esp_crt_bundle.h"

#include "LEDController_Task.h"

static const char* TAG_MQTTClient = "MQTTClient";

static void MQTTClient_EventHandlerWifi(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        ESP_LOGI(TAG_MQTTClient, "WIFI connect ...");
        esp_wifi_connect();
    } 
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        ESP_LOGI(TAG_MQTTClient, "WIFI disconnected");
        esp_wifi_connect();
    }
    else if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED)
    {
        ESP_LOGI(TAG_MQTTClient, "WIFI connected");
    }
}

static void MQTTClient_EventHandlerMQTT(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    MQTTClient_Data* data = handler_args;
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;

    char* topic = NULL;
    char* payload = NULL;

    switch ((esp_mqtt_event_id_t)event_id)
    {
        case MQTT_EVENT_CONNECTED:
            esp_mqtt_client_publish(client, "fablight", data->ID, 0, 0, 0);
            ESP_LOGI(TAG_MQTTClient, "MQTT_EVENT_CONNECTED");

            esp_mqtt_client_subscribe(client, data->Topic, 1);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG_MQTTClient, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG_MQTTClient, "MQTT_EVENT_DATA");

            ESP_LOGD(TAG_MQTTClient, "TOPIC=%.*s\r\n", event->topic_len, event->topic);
            ESP_LOGD(TAG_MQTTClient, "DATA=%.*s\r\n", event->data_len, event->data);

            topic = malloc(sizeof(char) * event->topic_len + 1);
            payload = malloc(sizeof(char) * event->data_len + 1);
            topic[event->topic_len] = 0;
            topic[event->data_len] = 0;

            strncpy(topic, event->topic, event->topic_len);
            strncpy(payload, event->data, event->data_len);

            char* fablight = strtok(topic, "/");
            if(fablight == NULL)
            {
                break;
            }

            char* id = strtok(NULL, "/");
            if(id == NULL)
            {
                break;
            }
            
            char* effect = strtok(NULL, "/");
            if(effect == NULL)
            {
                break;
            }

            char* segment = strtok(NULL, "/");
            if(segment == NULL)
            {
                break;
            }

            char* ring = strtok(NULL, "/");
            if(ring == NULL)
            {
                break;
            }

            LEDController_Command command = 
            {
                .Effect = -1,
                .Segment = -1,
                .Ring = -1,
                .Red = 0,
                .Green = 0,
                .Blue = 0
            };

            if(!strcmp(effect, "static"))
            {
                command.Effect = (enum LEDController_Effect)STATIC;
            }
            else
            {
                break;
            }

            command.Segment = atoi(segment);
            if(command.Segment < -1 && command.Segment > 99999)
            {
                break;
            }

            command.Ring = atoi(ring);
            if(command.Ring < -1 && command.Ring > 99999)
            {
                break;
            }

            if(event->data_len == strlen("FFFFFF"))
            {
                char r[3] = {0};
                char g[3] = {0};
                char b[3] = {0};

                strncpy(r, &(payload[0]), 2);
                strncpy(g, &(payload[2]), 2);
                strncpy(b, &(payload[4]), 2);
                
                command.Red = strtol(r, NULL, 16);
                command.Green = strtol(g, NULL, 16);
                command.Blue = strtol(b, NULL, 16);

                xQueueSend(*(data->Effect_Queue), (void*) &command, (TickType_t)0);
            }
            break;
        default:
            ESP_LOGD(TAG_MQTTClient, "Unhandled Event");
    }

    if(topic != NULL)
    {
        free(topic);
    }
    if(payload != NULL)
    {
        free(payload);
    }
}

void MQTTClient_Init(MQTTClient_Config* config, MQTTClient_Data* data)
{
    ESP_LOGI(TAG_MQTTClient, "Delay for Programming");
    vTaskDelay(10000 / portTICK_PERIOD_MS);

    data->Effect_Queue = config->Effect_Queue;
    data->ID = config->ID;
    sprintf(data->Topic, "fablight/%s/#", config->ID);
    //data->Topic = "fablight/#";
    ESP_LOGI(TAG_MQTTClient, "Init");

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t event_wifi;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &MQTTClient_EventHandlerWifi,
                                                        NULL,
                                                        &event_wifi));

    wifi_config_t wifi_config = 
    {
        .sta = 
        {
            //.threshold.authmode = WIFI_AUTH_WPA2_PSK
            .threshold.authmode = WIFI_AUTH_OPEN
        },
    };
    strcpy((char*)wifi_config.sta.ssid, config->SSID);
    strcpy((char*)wifi_config.sta.password, config->PSK);

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGW(TAG_MQTTClient, "%s", config->Host);
    esp_mqtt_client_config_t mqtt_cfg = 
    {
        .broker.address.uri = config->Host,
        .credentials.username = config->Username,
        .credentials.authentication.password = config->Password,

        .broker.verification.crt_bundle_attach = esp_crt_bundle_attach
    };
    data->client = esp_mqtt_client_init(&mqtt_cfg);

    esp_mqtt_client_register_event(data->client, ESP_EVENT_ANY_ID,  MQTTClient_EventHandlerMQTT, data);
    esp_mqtt_client_start(data->client);
}

void MQTTClient_Task(void *pvParameter)
{
    MQTTClient_Config* config = pvParameter;

    char topic[] = "fablight/00:00:00:00:00:00/#";
    MQTTClient_Data data;
    data.Topic = topic;

    MQTTClient_Init(config, &data);

    ESP_LOGI(TAG_MQTTClient, "Run");

    char topic_msg[] = "fablight/00:00:00:00:00:00";
    sprintf(topic_msg, "fablight/%s", data.ID);
    while(1)
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        ESP_LOGD(TAG_MQTTClient, "Update");
    }
}