#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include <string.h>

#include "esp_log.h"
#include "esp_netif.h"
#include "nvs_flash.h"

#include "LEDController_Task.h"
#include "SerialClient_Task.h"

static const char *TAG_MAIN = "MAIN";

void app_main(void)
{   
    ESP_LOGI(TAG_MAIN, "Init");

    int Segment_GPIOs[3] = {10, 12, 14};
    QueueHandle_t ledcontroller_queue = xQueueCreate(9+1, sizeof(LEDController_Command));
    LEDController_Config ledcontroller_config =
    {
        .Ring_Count = 10,
        .LED_Count = 2,
        .Segment_Count = sizeof(Segment_GPIOs) / sizeof(int),
        .Segment_Pins = Segment_GPIOs,
        .Effect_Queue = &ledcontroller_queue
    };
    xTaskCreate(&LEDController_Task, "LEDController_Task", 4096, (void*) &ledcontroller_config, 5, NULL);
    
    SerialClient_Config serialclient_config = 
    {
        .Effect_Queue = &ledcontroller_queue
    };
    xTaskCreate(&SerialClient_Task, "SerialClient_Task", 4096, (void*) &serialclient_config, 5, NULL);

    ESP_LOGI(TAG_MAIN, "Run");

    while(42)
    {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        ESP_LOGD(TAG_MAIN, "Update");
    }
}