#ifndef SERIAL_CLIENT_TASK
#define SERIAL_CLIENT_TASK

#include "led_strip.h"

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

typedef struct SerialClient_Config
{
    // Effect Queue
    QueueHandle_t* Effect_Queue;
} SerialClient_Config;

typedef struct SerialClient_Data
{
    // Effect Queue
    QueueHandle_t* Effect_Queue;
} SerialClient_Data;

void SerialClient_Init(SerialClient_Config* config, SerialClient_Data* data);
void SerialClient_Task(void *pvParameter);

#endif