#include "LEDController_Task.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "led_strip.h"

static const char* TAG_LEDCONTROLLER = "LEDCONTROLLER";

#define LED_STRIP_RMT_RES_HZ  (10 * 1000 * 1000)

void LEDController_Init(LEDController_Config* config, LEDController_Data* data)
{
    ESP_LOGI(TAG_LEDCONTROLLER, "Init");

    data->LED_Handle_Count = config->Segment_Count;
    data->LED_Handles = malloc(sizeof(led_strip_handle_t) * data->LED_Handle_Count);

    led_strip_rmt_config_t rmt_config = 
    {
        .clk_src = RMT_CLK_SRC_DEFAULT,
        .resolution_hz = LED_STRIP_RMT_RES_HZ,
        .flags.with_dma = false,
    };

    for(int i=0; i < config->Segment_Count; i++)
    {
        led_strip_config_t led_strip_config = 
        {
            .strip_gpio_num = config->Segment_Pins[i],
            .max_leds = config->Ring_Count * config->LED_Count,
            .led_pixel_format = LED_PIXEL_FORMAT_GRB,
            .led_model = LED_MODEL_WS2812,
            .flags.invert_out = false,
        };

        ESP_ERROR_CHECK(led_strip_new_rmt_device(&led_strip_config, &rmt_config, &data->LED_Handles[i]));
    }
}

void LEDController_Task(void *pvParameter)
{
    LEDController_Config* config = pvParameter;

    LEDController_Data data;

    LEDController_Init(config, &data);

    ESP_LOGI(TAG_LEDCONTROLLER, "Run");
    while(1)
    {
        LEDController_Command command;
        if(xQueueReceive(*(config->Effect_Queue), &command, 1000/portTICK_PERIOD_MS))
        {
            ESP_LOGD(TAG_LEDCONTROLLER, "Update");

            if(command.Segment >= config->Segment_Count || command.Segment < -1)
            {
                ESP_LOGW(TAG_LEDCONTROLLER, "SegmentID %d is invalid", command.Segment);
                continue;
            }
            if(command.Ring >= config->Ring_Count || command.Ring < -1)
            {
                ESP_LOGW(TAG_LEDCONTROLLER, "RingID %d is invalid", command.Ring);
                continue;
            }

            // TODO
            if(command.Effect > 0 || command.Effect < 0)
            {
                ESP_LOGW(TAG_LEDCONTROLLER, "EffectID %d is invalid", command.Effect);
                continue;
            }

            if(command.Effect == (enum LEDController_Effect)STATIC)
            {
                if(command.Segment == -1)
                {
                    for(int i=0; i < data.LED_Handle_Count; i++)
                    {
                        if(command.Ring == -1)
                        {
                            for(int e=0; e < config->Ring_Count * config->LED_Count; e++)
                            {
                                ESP_ERROR_CHECK(led_strip_set_pixel(data.LED_Handles[i], e, command.Red, command.Green, command.Blue));
                            }
                        }
                        else
                        {
                            for(int e=command.Ring * config->LED_Count; e < (command.Ring * config->LED_Count) + config->LED_Count; e++)
                            {
                                ESP_ERROR_CHECK(led_strip_set_pixel(data.LED_Handles[i], e, command.Red, command.Green, command.Blue));
                            }
                        }
                        ESP_ERROR_CHECK(led_strip_refresh(data.LED_Handles[i]));
                    }
                }
                else
                {
                    if(command.Ring == -1)
                    {
                        for(int e=0; e < config->Ring_Count * config->LED_Count; e++)
                        {
                            ESP_ERROR_CHECK(led_strip_set_pixel(data.LED_Handles[command.Segment], e, command.Red, command.Green, command.Blue));
                        }
                    }
                    else
                    {
                        for(int e=command.Ring * config->LED_Count; e < (command.Ring * config->LED_Count) + config->LED_Count; e++)
                        {
                            ESP_ERROR_CHECK(led_strip_set_pixel(data.LED_Handles[command.Segment], e, command.Red, command.Green, command.Blue));
                        }
                    }
                    ESP_ERROR_CHECK(led_strip_refresh(data.LED_Handles[command.Segment]));
                }
            }
        }       
    }
}