#include "SerialClient_Task.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#include "esp_log.h"
#include "esp_crt_bundle.h"
#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "esp_vfs_cdcacm.h"
#include "esp_timer.h"

#include "LEDController_Task.h"

static const char* TAG_SerialClient = "SerialClient";
const char* prompt = LOG_COLOR_I CONFIG_IDF_TARGET "> " LOG_RESET_COLOR;

SerialClient_Data data;

void Pomodoro_Task(void *pvParameter)
{
    SerialClient_Data* data2 = (SerialClient_Data*)pvParameter;

    LEDController_Command command = 
    {
        .Effect = (enum LEDController_Effect)STATIC,
        .Segment = -1,
        .Ring = -1,
        .Red = 0,
        .Green = 0,
        .Blue = 0
    };

    xQueueSend(*(data2->Effect_Queue), (void*) &command, (TickType_t)0);

    int64_t time_work = 1000 * 1000 * 60 * 25;
    int64_t time_free = 1000 * 1000 * 60 * 5;

    int64_t start_time = esp_timer_get_time();
    int state = 0;

    int led_state = 0;
    while(42)
    {
        if(state == 0)
        {
            if(start_time + time_work > esp_timer_get_time())
            {
                command.Segment = -1;
                command.Ring = led_state;
                command.Red = 255;

                xQueueSend(*(data2->Effect_Queue), (void*) &command, (TickType_t)0);

                if(led_state == 0)
                {
                    command.Ring = 2;
                }
                else
                {
                    command.Ring = led_state-1;
                }
                
                command.Red = 0;

                xQueueSend(*(data2->Effect_Queue), (void*) &command, (TickType_t)0);

                led_state++;
                
                if(led_state > 2)
                {
                    led_state = 0;
                }
            }
            else
            {
                start_time = esp_timer_get_time();
                state = 1;
                command.Red = 0;
            }
        }
        else
        {
            if(start_time + time_free > esp_timer_get_time())
            {
                command.Ring = -1;
                command.Segment = led_state;
                command.Green = 255;

                xQueueSend(*(data2->Effect_Queue), (void*) &command, (TickType_t)0);

                if(led_state == 0)
                {
                    command.Segment = 2;
                }
                else
                {
                    command.Segment = led_state-1;
                }
                command.Green = 0;

                xQueueSend(*(data2->Effect_Queue), (void*) &command, (TickType_t)0);

                led_state++;
                
                if(led_state > 2)
                {
                    led_state = 0;
                }
            }
            else
            {
                start_time = esp_timer_get_time();
                state = 0;
                command.Green = 0;
            }
        }

        vTaskDelay(1000 / portTICK_PERIOD_MS);
        ESP_LOGD(TAG_SerialClient, "Pomodoro");
    }
}

TaskHandle_t pomodorotask;

static int pomodoro_start(int argc, char **argv)
{
    ESP_LOGI(TAG_SerialClient, "Start Pomodoro");
    xTaskCreate(&Pomodoro_Task, "Pomodoro_Task", 4096, &data, 5, &pomodorotask);

    return 0;
}

static int pomodoro_stop(int argc, char **argv)
{
    ESP_LOGI(TAG_SerialClient, "Stop Pomodoro");

    vTaskDelete( pomodorotask );

    LEDController_Command command = 
    {
        .Effect = (enum LEDController_Effect)STATIC,
        .Segment = -1,
        .Ring = -1,
        .Red = 0,
        .Green = 0,
        .Blue = 0
    };

    xQueueSend(*(data.Effect_Queue), (void*) &command, (TickType_t)0);


    return 0;
}


static void register_pomodoro(void)
{
    const esp_console_cmd_t cmd = {
        .command = "pomodoro_start",
        .help = "Start Pomodoro",
        .hint = NULL,
        .func = &pomodoro_start,
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd) );

    const esp_console_cmd_t cmd2 = {
        .command = "pomodoro_stop",
        .help = "Stop Pomodoro",
        .hint = NULL,
        .func = &pomodoro_stop,
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd2) );
}

void SerialClient_Init(SerialClient_Config* config, SerialClient_Data* data)
{
    data->Effect_Queue = config->Effect_Queue;

    ESP_LOGI(TAG_SerialClient, "Init");

    /* Disable buffering on stdin */
    setvbuf(stdin, NULL, _IONBF, 0);

    /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
    esp_vfs_dev_cdcacm_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_cdcacm_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

    /* Enable non-blocking mode on stdin and stdout */
    fcntl(fileno(stdout), F_SETFL, 0);
    fcntl(fileno(stdin), F_SETFL, 0);

    /* Initialize the console */
    esp_console_config_t console_config = {
            .max_cmdline_args = 8,
            .max_cmdline_length = 256,
            .hint_color = atoi(LOG_COLOR_CYAN)
    };
    ESP_ERROR_CHECK( esp_console_init(&console_config) );

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);

    /* Set command history size */
    linenoiseHistorySetMaxLen(10);

    esp_console_register_help_command();

    register_pomodoro();
}



void SerialClient_Task(void *pvParameter)
{
    SerialClient_Config* config = pvParameter;

    SerialClient_Init(config, &data);

    LEDController_Command command = 
    {
        .Effect = (enum LEDController_Effect)STATIC,
        .Segment = -1,
        .Ring = -1,
        .Red = 0,
        .Green = 0,
        .Blue = 10
    };

    xQueueSend(*(data.Effect_Queue), (void*) &command, (TickType_t)0);

    ESP_LOGI(TAG_SerialClient, "Run");

    while(true) {
        /* Get a line using linenoise.
         * The line is returned when ENTER is pressed.
         */
        char* line = linenoise(prompt);
        if (line == NULL) { /* Ignore empty lines */
            continue;
        }
        /* Add the command to the history */
        linenoiseHistoryAdd(line);

        /* Try to run the command */
        int ret;
        esp_err_t err = esp_console_run(line, &ret);
        if (err == ESP_ERR_NOT_FOUND) {
            printf("Unrecognized command\n");
        } else if (err == ESP_ERR_INVALID_ARG) {
            // command was empty
        } else if (err == ESP_OK && ret != ESP_OK) {
            printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(ret));
        } else if (err != ESP_OK) {
            printf("Internal error: %s\n", esp_err_to_name(err));
        }
        /* linenoise allocates line buffer on the heap, so need to free it */
        linenoiseFree(line);
    }
}